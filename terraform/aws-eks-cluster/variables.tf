variable "region" {
    type        = string
    description = "Choose the desired region for the EKS Cluster"
    default     = "us-east-1"
}

variable "iam_role_name" {
    type        = string
    description = "Choose a name for the IAM Role."
    default     = "aria_eks_iam_role"
}

variable "eks_node_iam_role_name" {
  type        = string
  description = "Choose the name for the new IAM role for the EKS worker groups"
  default     = "aria-eks-node-iam-role"
}


variable "eks_cluster_name" {
    type        = string
    description = "Choose a name for the EKS Cluster."
    default     = "aria_eks_cluster"
}

variable "vpc_cidr" {
    type        = string
    description = "Choose a CIDR for the VPC."
    default     = "10.0.0.0/16"
}

variable "vpc_name" {
    type        = string
    description = "Choose a name for the new VPC."
    default     = "aria-eks-vpc"
}

variable "eks_kubernetes_version" {
    type        = string
    description = "Choose a Kubernetes version for the EKS cluster."
    default     = "1.25"
}

variable "eks_sg_name" {
    type        = string
    description = "Enter a Security Group name for cluster communication with worker nodes"
    default     = "aria-eks-sg"
}

variable "eks_node_group_name" {
    type        = string
    description = "Enter a name for the cluster node group."
    default     = "aria_eks_node_group"
}