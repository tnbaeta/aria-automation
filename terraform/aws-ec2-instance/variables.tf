variable "region" {
    type        = string
    description = "Choose the desired region for the EKS Cluster"
    default     = "us-east-1"
}
