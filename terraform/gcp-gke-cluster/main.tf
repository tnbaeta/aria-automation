terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.63.1"
    }
  }
}

provider "google" {
  project = var.projectId
  region  = var.region
}

resource "google_compute_network" "gkeVpc" {
  name                    = var.gkeVpcName
  auto_create_subnetworks = "false"
}

# Subnet
resource "google_compute_subnetwork" "gkeSubnet" {
  name          = var.gkeSubnetName
  region        = var.region
  network       = google_compute_network.gkeVpc.name
  ip_cidr_range = "10.10.0.0/24"
}

resource "google_container_cluster" "gkeCluster" {
  name     = var.gkeClusterName
  location = var.region

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1

  network    = google_compute_network.gkeVpc.name
  subnetwork = google_compute_subnetwork.gkeSubnet.name
}

# Separately Managed Node Pool
resource "google_container_node_pool" "gkePrimaryNodes" {
  name       = var.gkePrimaryNodesName
  location   = var.region
  cluster    = google_container_cluster.gkeCluster.name
  node_count = var.gkeNumNodes

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      env = var.projectId
    }

    # preemptible  = true
    machine_type = var.machineType
    tags         = ["gke-node", "${var.projectId}-gke"]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
}